# README #

Development environment for phpbbex 2.0 forum.

* Install VirtualBox (v4.3.x)
* Install Vagrant
* Install NFS Vagrant plugin (for Windows users only)

$ vagrant plugin install vagrant-winnfsd

$ git clone https://smatros@bitbucket.org/smatros/phpbbex2-puphpet.git phpbbex2

$ vagrant up

* Wait for provisioning...

* Add line into host file 192.168.33.2 phpbbex2.local

* Rename /install_forum into /install

* Go to http://phpbbex2.local and follow the instructions

## MySQL ##
* DB name: phpbbex
* DB user: phpbbex_u
* DB password: phpbbex_u

*See details in puphpet/config.yaml*

**Happy coding!**

*Created with puPHPet.*